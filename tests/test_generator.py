from unittest.mock import patch

import pytest
from pydantic.error_wrappers import ValidationError

from events_generator.core.generators import EventChoices, EventStage
from events_generator.core.models import EventType


@pytest.mark.parametrize("probabilities", [[1.0], [0.5, 0.5], [0.25, 0.2, 0.5, 0.05]])
def test_probabilities_working(probabilities):
    print(probabilities)
    EventChoices(__root__=[EventStage(name="foo", probability=x) for x in probabilities])


@pytest.mark.parametrize(
    "probabilities",
    [
        pytest.param([1.45], id="Higher than 1"),
        pytest.param([0.45], id="Lower than 1"),
        pytest.param([0.5, 0.25], id="Sum lower than 1"),
        pytest.param([0.25, 0.2, 0.5, 0.5], id="Sum higher than 1"),
    ],
)
def test_probabilities_failing(probabilities):
    with pytest.raises(ValidationError):
        EventChoices(__root__=[EventStage(name="foo", probability=x) for x in probabilities])


@pytest.mark.parametrize(
    "probability, expected",
    [
        (0.0, EventType.OrderCancelled),
        (0.1, EventType.OrderCancelled),
        (0.1999999, EventType.OrderCancelled),
        (0.2, EventType.OrderCancelled),
        (0.200001, EventType.OrderDelivered),
        (0.75, EventType.OrderDelivered),
        (1.0, EventType.OrderDelivered),
    ],
)
def test_random_draw(default_flow, probability, expected):
    with patch("random.random", side_effect=[0.34, probability]):
        events = default_flow.draw_event_chain()
        assert next(events).type_ == EventType.OrderPlaced
        assert next(events).type_ == expected
