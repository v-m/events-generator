from datetime import timedelta
from unittest.mock import patch, MagicMock

import pytest

from events_generator.core.writer import DirectoryStore


@pytest.mark.parametrize("nr_calls, file_rotations", [[0, 0], [1, 1], [10, 1], [11, 2],])
def test_add_event_batch_size(nr_calls, file_rotations):
    with patch.object(DirectoryStore, "_new_target_file") as mock:
        with DirectoryStore(10, None, MagicMock()) as d:
            for _ in range(nr_calls):
                d.add_event(MagicMock())

        assert mock.call_count == file_rotations


@pytest.mark.parametrize("interval, file_rotations", [[0, 1], [5, 1], [10, 1], [11, 2]])
def test_add_event_interval(interval, file_rotations):
    with patch.object(DirectoryStore, "_new_target_file") as mock:

        with DirectoryStore(None, 10, MagicMock()) as d:
            d.add_event(MagicMock())

            with patch("events_generator.core.writer.datetime") as dt_mock:
                dt_mock.now.return_value = d._timestamp + timedelta(seconds=interval)
                d.add_event(MagicMock())

                assert mock.call_count == file_rotations
