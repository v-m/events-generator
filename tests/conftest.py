import pytest

from events_generator.core.flow import load_flow


@pytest.fixture
def default_flow():
    return load_flow()
