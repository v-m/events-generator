import logging
import re
import sys
from abc import ABC
from datetime import datetime, timedelta
from pathlib import Path
from typing import Optional

from events_generator.core.models import Event

logger = logging.getLogger(__name__)

_DATE_SUBSTITUTION = re.compile("[^0-9]")


class StoreWriter(ABC):
    def add_event(self, event: Event):
        raise NotImplementedError


class StdoutStore(StoreWriter):
    """Write events to stdout"""

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass

    def add_event(self, event: Event):
        sys.stdout.write(event.json(by_alias=True))
        sys.stdout.write("\n")


class DirectoryStore(StoreWriter):
    """
    Write events to files, create a new file when either an interval has elapsed or
    the batch size is reached. If both are omitted, write all events on the same file.
    Use this class as a context manager to ensure proper resource cleaning.
    """

    def __init__(
        self,
        batch_size: Optional[int],
        interval: Optional[int],
        output_directory: Path,
        pattern_file: str = "events-{timestamp}.json",
    ):
        """
        :param batch_size: number of events per file (unlimited if omitted)
        :param interval: interval in seconds between each file (unlimited if omitted)
        :param output_directory: output directory for created files
        :param pattern_file: file name pattern
        """
        self._batch_size = batch_size
        self._interval = interval
        self._output_directory = output_directory
        self._pattern_file = pattern_file

        self._target_file = None
        self._current_file = None
        self._current_file_count = 0
        self._timestamp = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._close_current_file()

    def _close_current_file(self):
        """Closes the current file (if any open)"""
        if self._current_file is not None:
            logger.debug("Closing current output file %s", self._target_file)
            self._current_file.close()

    def _new_target_file(self) -> Path:
        """Closes the current file if any file open then open a new one"""
        self._close_current_file()
        timestamp = _DATE_SUBSTITUTION.sub("-", self._timestamp.isoformat())
        return self._output_directory / self._pattern_file.format(timestamp=timestamp)

    def _rotate_file(self):
        """Proceed to file rotation"""
        self._timestamp = datetime.now()
        self._target_file = self._new_target_file()
        logger.debug("Opening new file %s", self._target_file)
        self._current_file = self._target_file.open("w")
        self._current_file_count = 0

    def add_event(self, event: Event):
        """
        Proceed to file rotation if needed.
        add an event to the currently opened file.
        """
        if (
            self._current_file is None
            or (
                self._interval
                and datetime.now() > self._timestamp + timedelta(seconds=self._interval)
            )
            or (self._batch_size and self._current_file_count >= self._batch_size - 1)
        ):
            self._rotate_file()
        else:
            self._current_file.write(event.json(by_alias=True))
            self._current_file.write("\n")
            self._current_file_count += 1
