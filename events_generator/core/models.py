import uuid
from datetime import datetime
from enum import Enum

from pydantic import Field, BaseModel
from pydantic.schema import UUID


class EventType(Enum):
    OrderPlaced = "OrderPlaced"
    OrderDelivered = "OrderDelivered"
    OrderCancelled = "OrderCancelled"
    OrderAccepted = "OrderAccepted"
    OrderInTransit = "OrderInTransit"
    OrderBounced = "OrderBounced"
    OrderDeclined = "OrderDeclined"


class Order(BaseModel):
    order_id: UUID = Field(default_factory=uuid.uuid4, alias="OrderId")
    timestamp: datetime = Field(default_factory=datetime.now, alias="TimestampUtc")

    def generate_event(self, type_: str):
        return Event(type_=type_, data=self)

    class Config:
        allow_population_by_field_name = True


class Event(BaseModel):
    type_: EventType = Field(..., alias="Type")
    data: Order = Field(..., alias="Data")

    class Config:
        allow_population_by_field_name = True
