"""
Flow description using stages and multiple choices description.
Stages and choices are combined together to form an acyclic graph which describes how events are generated
as well as their probability of occurrence. Thus, an event choice contains a list of event stages from
which the next stage can be picked. This next stage contains a new optional choice link which describes the next
possible links.

Example representation of a choice acyclic graph:

choice -> stage 1 -> next choice -> stage 1a -> /
                                 -> stage 2b -> /
       -> stage 2 -> /
"""
import logging
import random
from typing import List, Optional, Iterable

from pydantic import BaseModel, validator

from events_generator.core.models import Event, Order

logger = logging.getLogger(__name__)


class EventStage(BaseModel):
    """
    One event stage in an event flow
    """

    name: str
    probability: float = 1.0
    next: Optional["EventChoices"] = None


class EventChoices(BaseModel):
    """
    List of possible event stage reachable from a specific stage.
    """

    __root__: List[EventStage]

    @validator("__root__")
    def probability_sum_check(cls, v):
        if sum(map(lambda x: x.probability, v)) != 1.0:
            raise ValueError("Total sum of probabilities for a choice should be 1")
        return v

    def draw_event_chain(self) -> Iterable[Event]:
        random_order = Order()
        drawn_value = random.random()

        total = 0
        for stage in self.__root__:
            if (new_total := total + stage.probability) >= drawn_value:
                yield Event(type_=stage.name, data=random_order)

                if stage.next:
                    yield from stage.next.draw_event_chain()
                break
            total = new_total


EventStage.update_forward_refs()
