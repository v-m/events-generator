import pkgutil
from pathlib import Path
from typing import Optional

import yaml

from events_generator.core.generators import EventChoices


def load_flow(generation_flow: Optional[Path] = None) -> EventChoices:
    if generation_flow:
        flow = generation_flow.read_text()
    else:
        flow = pkgutil.get_data("config", "flow.yaml")

    data = yaml.safe_load(flow)
    return EventChoices.parse_obj(data)
