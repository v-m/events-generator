import logging
from typing import Optional

import typer
from pydantic.parse import Path

from events_generator.core.flow import load_flow
from events_generator.core.writer import StdoutStore, DirectoryStore

logger = logging.getLogger(__name__)
app = typer.Typer(add_completion=False)


@app.command()
def generate(
    number_of_orders: int = typer.Option(..., help="Number of orders to generate"),
    batch_size: Optional[int] = typer.Option(
        None, help="Number of events per file (unlimited if omitted)"
    ),
    interval: Optional[int] = typer.Option(
        None, help="Interval in seconds between each file (unlimited if omitted)"
    ),
    output_directory: Optional[Path] = typer.Option(
        None, file_okay=False, help="Output directory for created files"
    ),
    create_directory: bool = typer.Option(
        False, "--create-directory", help="Create the output directory if it does not exist"
    ),
    output_files_pattern: Optional[str] = typer.Option(
        "orders-{timestamp}.json", help="Pattern for file name created"
    ),
    generation_flow: Optional[Path] = typer.Option(
        None, dir_okay=False, exists=True, help="Generation flow description"
    ),
):
    """
    Generate [number_of_orders] order events composed of a set of logical events described accordingly
    to a flow (defined in a config file).
    """
    logger.info(
        "Starting with configuration: #orders = %s, batch_size = %s, interval = %s. "
        "Output directory = %s, create directory = %s, output file patterns = %s, generation flow = %s",
        number_of_orders,
        batch_size,
        interval,
        output_directory,
        create_directory,
        output_files_pattern,
        generation_flow,
    )

    # Let's try to create the directory in case it does not exist
    if output_directory:
        if not output_directory.exists():
            if create_directory:
                logger.info("Target directory non existent. Creating %s...", output_directory)
                output_directory.mkdir(exist_ok=True)
            else:
                logger.error(
                    "Target directory %s non existent. Impossible to proceed...", output_directory
                )
                exit(1)

        generation_target = DirectoryStore(
            batch_size, interval, output_directory, output_files_pattern
        )
    else:
        logger.warning(
            "No output directory defined. "
            "Using stdout as a default, omitting --batch-size and --interval."
        )
        generation_target = StdoutStore()

    ch = load_flow(generation_flow)

    with generation_target as dir_:
        for _ in range(number_of_orders):
            for event in ch.draw_event_chain():
                dir_.add_event(event)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    app()
