# Events Generator

This project simulates a large batch of generated events
based on a config file describing probability of an event 
to occur.

Produced events are either dumped on `stdout` or on one or several files.

## Installation

This project requires Python 3.8. 
You can either create a virtual environment manually or use poetry.

```
> poetry install --no-dev
> poetry shell

# or, without poetry:
> pip install .

# or, without cloning the repo:
> pip install git+https://gitlab.com/v-m/events-generator.git
```

## Run it

The command can be run using the `generate-events` command.
Note that the generated events are not written in a valid JSON file but are appended line by line.

An execution example is:

```
generate-events \
    --number-of-orders=1000000 \
    --batch-size=5000 \
    --interval=1 \
    --output-directory=/tmp
```

Full command help can be obtained using the `--help` flag:

```
> generate-events --help
Usage: generate-events [OPTIONS]

  Generate [number_of_orders] orders composed of a set of logical event
  described accordingly to a flow (defined as an external flow file).

Options:
  --number-of-orders INTEGER    Number of orders to generate  [required]
  --batch-size INTEGER          Number of events per file (unlimited if
                                omitted)

  --interval INTEGER            Interval in seconds between each file
                                (unlimited if omitted)

  --output-directory DIRECTORY  Output directory for created files
  --create-directory            Create the output directory if it does not
                                exist  [default: False]

  --output-files-pattern TEXT   Pattern for file name created  [default:
                                orders-{timestamp}.json]

  --generation-flow FILE        Generation flow description
  --help                        Show this message and exit.
```

## Event Flows

The way events are generated is described in a flow description file (a YAML file).
This file uses stages and choices description to define the next event type and its probability to occur.
A choice contains a list of stages from which the next stage can be picked. 
This next stage contains a new optional choice link which describes the next
possible group.

The default flow is defined in [`config/flow.yaml`](config/flow.yaml) and is loaded by default if no flow is defined.
Another flow can be defined and passed to the program using the `--generation-flow` option.
However, bear in mind that the flow can only use events defined in the `events_generator.models.EventType` enumeration.
The value is checked, so adding new events types required to add them in the enumeration.
An example of a more complex flow can be found in [`config/flow2.yaml`](config/flow2.yaml) and executed as:

```
> generate-events \
    --number-of-orders=1000000 \
    --generation-flow="config/flow2.yaml"
```

Note that omitting a production directory will result in producing directly to `stdout`.

## Running tests

To run the tests, use `pytest` as:

```
# Install dev dependencies
> poetry install
> poetry run pytest

# Or without poetry:
> pip install pytest
> pytest
```

## Improvements

- Support generations with wait time between events. 
Events could occur asynchronously and overlaps each others.
- Quality improvement (eg. better testing coverage)